---
- name: Load distro variables
  include_vars: "{{ ansible_os_family }}.yml"

- name: Assert that ioc_nonvolatile_server is defined
  assert:
    that:
      - ioc_nonvolatile_server is defined

- name: Install required packages
  package:
    name: "{{ ioc_packages + ioc_dev_packages + ioc_prometheus_exporter_packages + ioc_host_packages }}"
    state: present

- name: Install pip packages required by prometheus exporter
  pip:
    executable: "{{ ioc_pip_exec }}"
    extra_args: "-i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple"
    name: "{{ ioc_pip_packages }}"

- name: Create prometheus exporter user
  user:
    name: "{{ ioc_prometheus_exporter_user }}"
    system: true

- name: Create ioc exporter directory
  file:
    path: "{{ ioc_prometheus_exporter_directory }}"
    owner: "{{ ioc_prometheus_exporter_user }}"
    mode: 0755
    state: directory

- name: Create /var/log/prometheus directory
  file:
    path: /var/log/prometheus
    owner: "{{ ioc_prometheus_exporter_user }}"
    mode: 0755
    state: directory

- name: Add ioc exporter scripts
  copy:
    src: "ioc-exporter.py"
    dest: "{{ ioc_prometheus_exporter_directory }}/"
    owner: "{{ ioc_prometheus_exporter_user }}"
    mode: 0744

- name: Disable git safe.directory checks for prometheus
  become: true
  become_user: "{{ ioc_prometheus_exporter_user }}"
  git_config:
    name: safe.directory
    value: "*"
    scope: global
  when: "ansible_os_family in ['Concurrent CPU SDK', 'Debian']"

- name: Create service for ioc exporter
  template:
    src: "ioc-exporter.service.j2"
    dest: "/etc/systemd/system/ioc-exporter.service"
    owner: root
    group: root
    mode: 0644
  notify:
    - reload systemd config
    - restart prometheus exporters

- name: Ensure ioc exporter service is enabled and started
  service:
    name: "ioc-exporter"
    state: started
    enabled: true
    daemon-reload: true

- name: Create the group for the IOC user
  group:
    name: "{{ ioc_group_name }}"
    gid: "{{ ioc_group_id }}"

- name: Get group entries
  getent:
    database: group

- name: Create the IOC user for running IOCs
  user:
    name: "{{ ioc_user_name }}"
    uid: "{{ ioc_user_id }}"
    group: "{{ ioc_group_name }}"
    groups: "{{ ioc_user_groups + getent_group | select('equalto', 'realtime') | list }}"
    system: true

- name: Copy default IOC console configuration
  template:
    src: conserver_group.j2
    dest: /etc/conserver/00-ioc-group.cf
    owner: "{{ ioc_user_name }}"
    group: "{{ ioc_group_name }}"
    mode: 0644
  notify: reload conserver

# See https://epics-controls.org/resources-and-support/documents/howto-documents/channel-access-reach-multiple-soft-iocs-linux/
# We use ifup-local because NetworkManager is not enabled
- name: Create script to convert UDP CA incoming requests to broadcast
  template:
    src: ifup-local.j2
    dest: /sbin/ifup-local
    owner: root
    group: root
    mode: 0755
  tags:
    - ioc-iptables
  notify:
    - run ifup-local

- name: Create /var/log/procServ directory
  file:
    path: /var/log/procServ
    owner: "{{ ioc_user_name }}"
    group: "{{ ioc_group_name }}"
    mode: 0755
    state: directory

- name: Configure logrotate for procServ
  copy:
    src: procserv
    dest: /etc/logrotate.d/
    owner: root
    group: root
    mode: 0644

- name: Create IOC parent-directory ({{ ioc_iocs_folder }}) and set permissions
  become: true
  file:
    path: "{{ ioc_iocs_folder }}"
    owner: "{{ ioc_user_name }}"
    group: "{{ ioc_group_name }}"
    mode: 0755
    state: directory

- name: Create IOC runtime-directory ({{ ioc_runtime_folder }}) and set permissions
  become: true
  file:
    path: "{{ ioc_runtime_folder }}"
    owner: "{{ ioc_user_name }}"
    group: "{{ ioc_group_name }}"
    mode: 0755
    state: directory

- name: Add global core gitignore file
  copy:
    src: "gitignore_core"
    dest: "{{ ioc_iocs_folder }}/.gitignore_core"
    owner: "{{ ioc_user_name }}"
    mode: 0744

- name: Globally ignore core files
  git_config:
    name: core.excludesfile
    scope: system
    value: "{{ ioc_iocs_folder }}/.gitignore_core"

- name: Set coredump pattern
  sysctl:
    name: kernel.core_pattern
    value: "core"
    state: present
    sysctl_file: /etc/sysctl.d/ioc-coredump-handler.conf
    reload: true

- name: Enable coredump handler
  sysctl:
    name: kernel.core_uses_pid
    value: "0"
    state: present
    sysctl_file: /etc/sysctl.d/ioc-coredump-handler.conf
    reload: true

- name: Flush handlers
  meta: flush_handlers
  tags:
    - ioc-update

- name: Remove IOCs
  include_tasks: remove_ioc.yml
  tags:
    - ioc-update

- name: Filter IOCs to deploy
  set_fact:
    iocs_to_deploy: "{{ iocs | selectattr('name', 'in', names_of_iocs_to_deploy) | list }}"

- name: Create IOCs
  include_tasks: deploy_ioc.yml
  tags:
    - ioc-update

- name: Fetch deployed IOC metadata
  block:
    - name: Collect IOC metadata files
      find:
        paths:
          - "{{ ioc_runtime_folder }}"
        recurse: true
        patterns:
          - "metadata.json"
      register: cloned_ioc_metadata
      tags:
        - ioc-update

    - name: Read in deployed IOC metadata
      slurp:
        src: "{{ item }}"
      loop: "{{ cloned_ioc_metadata.files | map(attribute='path') | list }}"
      register: _slurp_metadata
      tags:
        - ioc-update

    - name: Parse deployed IOC metadata
      set_fact:
        deployed_iocs: "{{ _slurp_metadata['results'] | \
          map(attribute='content') | \
          map('b64decode') | \
          map('from_json') | list }}"
      tags:
        - ioc-update

- name: Update MOTD
  template:
    src: motd.j2
    dest: /etc/motd
    owner: root
    group: root
    mode: 0644
  tags:
    - ioc-update
