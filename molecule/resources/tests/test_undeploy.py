import os

import testinfra.utils.ansible_runner

from .utils import service_is_enabled


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("ioc_hosts")


IOCSLUG = "ioc_to_undeploy"


def test_undeployed_ioc_service_removed(host):
    opt_iocs = "/opt/iocs"
    opt_nonv = "/opt/nonvolatile"

    assert not host.file(os.path.join(opt_iocs, IOCSLUG)).exists
    assert not host.file(os.path.join(opt_nonv, IOCSLUG)).exists


def test_undeployed_ioc_procserv_log_remains(host):
    assert host.file(f"/var/log/procServ/out-{IOCSLUG}.log").is_file


def test_undeployed_ioc_no_longer_running(host):
    service = host.service(f"{IOCSLUG}")
    if "ioc_esslinux" in host.ansible.get_variables()["groups"]:
        assert not service_is_enabled(host, f"ioc-{IOCSLUG}")
    else:
        assert not service.is_enabled
    assert not service.is_running
