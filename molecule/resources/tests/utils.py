import re

CAGET = "/tmp/caget"


# On Yocto find is from busybox and makes the standard infra Service.is_enable to fail
def service_is_enabled(host, service):
    cmd = host.run_test(f"systemctl is-enabled {service}")
    return cmd.rc == 0


def caget(host, pv):
    return host.run(f"{CAGET} -w 5 -t {pv}")


def sluggify(ioc):
    return re.sub("[^a-zA-Z0-9_-]", "_", ioc)
