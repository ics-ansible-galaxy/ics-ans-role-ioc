import os
import re
import stat
import time

import pytest
import testinfra.utils.ansible_runner

from .utils import caget, service_is_enabled, sluggify

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("ioc_hosts")

IOCUSER_USERNAME = "iocuser"
IOCGROUP_GROUPNAME = "iocgroup"


def test_conserver_reports_all_consoles(host):
    if host.system_info.distribution == "ubuntu":
        conserver = host.service("conserver-server")
    else:
        conserver = host.service("conserver")
    assert conserver.is_running
    cmd = host.run("console -u")
    assert cmd.rc == 0

    # Note that the output of conserver is a series of rows such as
    #
    #   test-IOC:1                  up
    #   test-IOC:2                down
    #
    # and we don't care about the up/down state
    consoles = cmd.stdout.split()[::3]
    assert set(consoles) == set(
        sluggify(ioc)
        for ioc in [
            "test-IOC:conda",
            "test-IOC:address-list",
            "test-IOC:pkg-installs",
            "test-IOC:nfs",
        ]
    )


@pytest.mark.parametrize(
    "ioc",
    [
        "test-IOC:conda",
        "test-IOC:address-list",
        "test-IOC:pkg-installs",
        "test-IOC:nfs",
    ],
)
def test_ioc_services_are_running(host, ioc):
    ioc_service = host.service(f"ioc-{sluggify(ioc)}")
    if "ioc_esslinux" in host.ansible.get_variables()["groups"]:
        assert service_is_enabled(host, f"ioc-{sluggify(ioc)}")
    else:
        assert ioc_service.is_enabled
    assert ioc_service.is_running


def test_packages_are_installed(host):
    if "ioc_esslinux" in host.ansible.get_variables()["groups"]:
        pytest.skip("Unsupported host")
    if host.system_info.distribution != "ubuntu":
        assert host.package("procServ").is_installed
        assert host.package("tmux").is_installed

        # ioc_host_packages
        assert host.package("ImageMagick-devel").is_installed


def test_ioc_service_is_configured(host):
    iocname = "test-IOC:conda"
    with host.sudo(IOCUSER_USERNAME):
        script = host.file(f"/etc/systemd/system/ioc-{sluggify(iocname)}.service")
        assert script.contains('Environment="LOG_SERVER_NAME=logs.example.com"')
        assert script.contains(f'Environment="IOCNAME={iocname}"')


def test_iocuser_is_present(host):
    user = host.user(IOCUSER_USERNAME)
    assert user.uid == 1042


def test_nfs_mounts_are_mounted(host):
    assert host.mount_point("/epics").exists


def test_nfs_nonvolatile_is_mounted_for_default_ioc(host):
    mount_source = "ics-ans-role-ioc-resource-nonvolatile:/export/nonvolatile"
    iocname = "test-IOC:conda"

    opt_nonv = f"/opt/nonvolatile/{sluggify(iocname)}"
    assert host.file(opt_nonv).is_directory
    assert host.mount_point(opt_nonv).filesystem == "nfs4"
    assert host.mount_point(opt_nonv).device == f"{mount_source}/{sluggify(iocname)}"


def test_nfs_nonvolatile_is_writeable(host):
    iocname = "test-IOC:conda"
    nonvolatile_path = "/opt/nonvolatile"
    assert host.file(f"{nonvolatile_path}/{sluggify(iocname)}").user == IOCUSER_USERNAME

    # Check that file is writeable by owner
    assert host.file(f"{nonvolatile_path}/{sluggify(iocname)}").mode & stat.S_IWUSR


def test_prometheus_ioc_exporter_reports_correct_on_unchanged_repos(host):
    iocname = "test-IOC:conda"
    ioc_exporter_port = "12110"
    cmd = host.command(f"curl localhost:{ioc_exporter_port}")
    assert f'ioc_repository_dirty{{ioc="{iocname}"}} 0.0' in cmd.stdout
    assert f'ioc_repository_local_commits{{ioc="{iocname}"}} 0.0' in cmd.stdout

    assert (
        f'ioc_essioc_missing{{ioc="{iocname}"}}' in cmd.stdout
    )  # we do not care about result since usage varies for test IOCs
    assert (
        f'ioc_cellmode_used{{ioc="{iocname}"}}' in cmd.stdout
    )  # we do not care about result since usage varies for test IOCs


def test_prometheus_ioc_exporter_reports_dirty_repository(host):
    iocname = "test-IOC:nfs"
    ioc_exporter_port = "12110"
    cmd = host.command(f"curl localhost:{ioc_exporter_port}")
    assert f'ioc_repository_dirty{{ioc="{iocname}"}} 1.0' in cmd.stdout


def test_prometheus_ioc_exporter_reports_local_commits(host):
    iocname = "test-IOC:pkg-installs"
    ioc_exporter_port = "12110"
    cmd = host.command(f"curl localhost:{ioc_exporter_port}")
    assert f'ioc_repository_local_commits{{ioc="{iocname}"}} 1.0' in cmd.stdout


@pytest.mark.parametrize("ioc", ["test-IOC:conda", "test-IOC:nfs"])
def test_ioc_is_reachable_via_ca(host, ioc):
    cmd = caget(host, f"{ioc}:BaseVersion")
    assert cmd.succeeded


@pytest.mark.parametrize(
    "ioc, version", [("test-IOC:nfs", "7.0.7"), ("test-IOC:address-list", "7.0.6.1")]
)
def test_ioc_loads_correct_version_of_epics_base(host, ioc, version):
    cmd = caget(host, f"{ioc}:BaseVersion")
    assert cmd.succeeded
    assert cmd.stdout.startswith(version)


def test_address_list_is_in_environment(host):
    iocname = sluggify("test-IOC:address-list")
    ioc_log = host.file(f"/var/log/procServ/out-{iocname}.log")
    addr_list = "127.0.0.1 1.2.3.4"
    assert ioc_log.contains(rf'EPICS_CA_ADDR_LIST\s*=\s*"{addr_list}"')
    # assert ioc_log.contains(f"EPICS_PVA_ADDR_LIST={addr_list}")


def test_procserv_dir_permissions(host):
    procserv_dir = host.file("/var/log/procServ")
    assert procserv_dir.is_directory
    assert procserv_dir.mode == 0o0755
    assert procserv_dir.user == IOCUSER_USERNAME
    assert procserv_dir.group == IOCGROUP_GROUPNAME


def test_procserv_log_permissions(host):
    iocname = "test-IOC:nfs"
    procserv_file = host.file(f"/var/log/procServ/out-{sluggify(iocname)}.log")
    assert procserv_file.is_file
    assert procserv_file.mode == 0o0644
    assert procserv_file.user == IOCUSER_USERNAME
    assert procserv_file.group == IOCGROUP_GROUPNAME


@pytest.mark.parametrize(
    "ioc, check", [("test-IOC:conda", True), ("test-IOC:nfs", False)]
)
def test_tag_to_same_commit_restarts_ioc(host, ioc, check):
    old_pid = host.file(f"/tmp/{ioc}_pid").content_string
    cmd = caget(host, f"{ioc}:PROCESS_ID")
    assert cmd.succeeded
    new_pid = cmd.stdout

    assert (old_pid == new_pid) == check


def test_iocversion_is_readable_by_iocstats(host):
    iocname = "test-IOC:conda"
    cmd = caget(host, f"{iocname}:IOCVERSION")
    assert cmd.succeeded
    assert cmd.stdout.strip() == "master"


# Note that this test _must_ come before test_segfault below, since that will
# also restart the IOC.
def test_systemd_restarts_procserv(host):
    ioc = sluggify("test-IOC:nfs")
    pid_re = re.compile(r"(?<=^pid:)\d+", re.MULTILINE)

    pid = pid_re.search(host.file(f"/run/ioc@{ioc}/info").content_string).group()
    if "ioc_esslinux" in host.ansible.get_variables()["groups"]:
        # host.process.get uses a specific ps arguments that are not available on
        # ESS linux because of busybox.
        cmd = host.run(f"ps | grep {pid}")
        assert "procServ" in cmd.stdout
    else:
        assert host.process.get(pid=pid).comm == "procServ"

    # Kill procServ
    with host.sudo():
        host.run(f"kill {pid}")

    # Wait some time to make sure procServ was killed
    time.sleep(2)

    # Assert that the process has been killed
    try:
        if "ioc_esslinux" in host.ansible.get_variables()["groups"]:
            cmd = host.run(f"ps | awk '{{print $1}};' | grep {pid}")
            assert cmd.rc == 1
        else:
            host.process.get(pid=pid)
            assert False
    except RuntimeError:
        pass

    # Give some time to systemd to restart and procServ to start
    time.sleep(2)

    new_pid = pid_re.search(host.file(f"/run/ioc@{ioc}/info").content_string).group()
    if "ioc_esslinux" in host.ansible.get_variables()["groups"]:
        cmd = host.run(f"ps | grep {new_pid}")
        assert "procServ" in cmd.stdout
    else:
        assert host.process.get(pid=new_pid).comm == "procServ"


def test_segfault_generates_coredump_file_without_any_suffix(host):
    iocname = "test-IOC:nfs"
    ioc_dir = f"/opt/iocs/{sluggify(iocname)}"
    core_file = f"{ioc_dir}/core"

    assert not host.file(core_file).exists

    cmd = caget(host, f"{iocname}:PROCESS_ID")
    assert cmd.rc == 0

    # Cause a segmentation fault
    pid = int(cmd.stdout)
    with host.sudo():
        host.command(f"kill -s SEGV {pid}")
    # Give some time to dump core
    time.sleep(2)

    assert host.file(core_file).is_file

    # Ensure that the repo is still clean
    git_status = host.run(f"git -C {ioc_dir} status --porcelain")
    assert "core" not in git_status.stdout


@pytest.mark.parametrize("iocname", ["test-IOC:conda", "test-IOC:nfs"])
def test_iocsh_history_file_is_missing(host, iocname):
    iocsh_history_path = f"/opt/iocs/{sluggify(iocname)}/.iocsh_history"

    assert not host.file(iocsh_history_path).exists
