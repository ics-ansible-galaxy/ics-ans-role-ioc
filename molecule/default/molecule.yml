---
dependency:
  name: galaxy
  options:
    role-file: molecule/resources/requirements.yml
lint: |
  set -e
  yamllint .
  ANSIBLE_ROLES_PATH=${MOLECULE_EPHEMERAL_DIRECTORY}/roles ansible-lint
  flake8
provisioner:
  name: ansible
  playbooks:
    prepare: ../resources/prepare.yml
    converge: ../resources/converge.yml
    side_effect: ../resources/side_effect.yml
  config_options:
    defaults:
      gather_timeout: 20
  inventory:
    host_vars:
      ics-ans-role-ioc-resource-git:
        iocs:
          - repo_name: e3-ioc-conda
            st_cmd: |
              require essioc

              iocshLoad $${essioc_DIR}/common_config.iocsh

              iocInit

              echo "$${IOCNAME}"
            ioc_json:
              ioc_type: "conda"
            environment_yaml: |
              dependencies:
              - epics-base
              - essioc

          - repo_name: e3-ioc-address-list
            ioc_json:
              address_list: ["127.0.0.1", "1.2.3.4"]
              ioc_type: nfs
              epics_version: 7.0.6.1
              require_version: 4.0.0
          - repo_name: e3-ioc-pkg-installs
            ioc_json:
              ioc_type: "conda"
            environment_yaml: |
              dependencies:
              - epics-base
              - iocstats
          - repo_name: e3-ioc-nfs
            st_cmd: |
              require essioc
              iocshLoad $${essioc_DIR}/common_config.iocsh

              iocInit

              echo "$${IOCNAME}"
      ics-ans-role-ioc-default:
        ioc_nonvolatile_path: "/opt/nonvolatile"
        ioc_nonvolatile_server_path: "/export/nonvolatile"
        ioc_nonvolatile_server: ics-ans-role-ioc-resource-nonvolatile
        ioc_nonvolatile_mount_fstype: "nfs"
        ioc_nonvolatile_mount_opts: "rw"
        ioc_nfs_mountpoints:
          - {
            src: "ics-ans-role-ioc-resource-nfs:/epics",
            path: "/epics",
            fstype: "nfs",
            opts: "ro",
          }
        ioc_dev_packages:
          - tmux
          - screen
        ioc_host_packages: ["ImageMagick-devel"]
        ioc_log_server_name: logs.example.com

scenario:
  name: default
verifier:
  name: testinfra
  directory: ../resources/tests/
driver:
  name: vagrant
  provider:
    name: virtualbox

platforms:
  - name: ics-ans-role-ioc-default
    box: esss/centos-7
    memory: 8192
    cpus: 2
    instance_raw_config_args:
      - vbguest.auto_update = false
      - "vm.network 'private_network', ip: '1.1.1.1', virtualbox__intnet: 'molecule_default_internal'"
    groups:
      - ioc_hosts
    networks:
      - name: ics-ans-role-ioc-default

  - name: ics-ans-role-ioc-resource-git
    box: esss/centos-7
    memory: 512
    cpus: 1
    instance_raw_config_args:
      - vbguest.auto_update = false
      - vm.hostname = 'ics-ans-role-ioc-resource-git'
      - "vm.network 'private_network', ip: '1.1.1.99', virtualbox__intnet: 'molecule_default_internal', hostname: true"
    groups:
      - git_daemon
    networks:
      - name: ics-ans-role-ioc-default

  - name: ics-ans-role-ioc-resource-nonvolatile
    box: esss/centos-7
    memory: 512
    cpus: 1
    instance_raw_config_args:
      - vbguest.auto_update = false
      - vm.hostname = 'ics-ans-role-ioc-resource-nonvolatile'
      - "vm.network 'private_network', ip: '1.1.1.98', virtualbox__intnet: 'molecule_default_internal', hostname: true"
    groups:
      - nonvolatile
    networks:
      - name: ics-ans-role-ioc-default

  - name: nfs
    box: https://artifactory.esss.lu.se/artifactory/e3-vagrant-local/e3.box
    memory: 512
    cpus: 1
    instance_raw_config_args:
      - vbguest.auto_update = false
      - vm.hostname = 'ics-ans-role-ioc-resource-nfs'
      - "vm.network 'private_network', ip: '1.1.1.97', virtualbox__intnet: 'molecule_default_internal', hostname: true"
    groups:
      - "nfs_hosts"
    networks:
      - name: ics-ans-role-ioc-default
